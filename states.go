package main

import (
	tea "github.com/charmbracelet/bubbletea"
	"time"
)

type State interface {
	Update(msg tea.Msg) (State, tea.Cmd)
	View() string
}

type StartState struct {
	Model    Model
	cursor   int
	selected map[int]struct{}
	index    int
}

func (s *StartState) Update(msg tea.Msg) (State, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "up", "k":
			if s.cursor > 0 {
				s.cursor--
			}
		case "down", "j":
			if s.cursor < len(s.Model.tests)-1 {
				s.cursor++
			}
		case "enter":
			s.index = s.cursor
			selectedTest := s.Model.tests[s.cursor]
			return &TypingState{Model: s.Model, Test: selectedTest}, nil
		case "ctrl+c":
			return s, tea.Quit
		}
	}
	return s, cmd
}
func (s *StartState) View() string {
	return startScreen(s.Model.styles, s.Model.width, s.Model.height, s, s.Model.tests)
}

type TypingState struct {
	Model     Model
	Test      TypingTest
	startTime time.Time
}

func (s *TypingState) Update(msg tea.Msg) (State, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.KeyMsg:
		if s.startTime.IsZero() {
			s.startTime = time.Now()
		}
		switch msg.String() {
		case "ctrl+c":
			return s, tea.Quit
		default:
			s.Test.UserInput, cmd = s.Test.UserInput.Update(msg)

			// Check if the user has typed the complete text
			if len(s.Test.UserInput.Value()) == len(s.Test.SampleText) {
				duration := time.Since(s.startTime)
				wpm := calculateWPM(s.Test.UserInput.Value(), duration)
				errorRate := calculateErrorRate(s.Test.SampleText, s.Test.UserInput.Value())
				return &ResultState{Model: s.Model, Result: Result{WPM: wpm, ErrorRate: errorRate}}, nil
			}

		}
	}
	return s, cmd
}

func (s *TypingState) View() string {
	return typingView(s.Test.SampleText, s.Test.UserInput, s.Model.styles, s.Model.width, s.Model.height)
}

type ResultState struct {
	Model  Model
	Result Result
}

func (s *ResultState) Update(msg tea.Msg) (State, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "q":
			return s, tea.Quit
		case "r":
			return &StartState{Model: s.Model}, nil
		}
	}
	return s, nil
}

func (s *ResultState) View() string {
	return resultView(s.Result, s.Model.styles, s.Model.width, s.Model.height)
}
