package main

import (
	"strings"
	"time"
)


func calculateWPM(input string, duration time.Duration) int {
	words := len(strings.Split(input, " "))
	minutes := duration.Minutes()
	return int(float64(words) / minutes)
}

func calculateErrorRate(original, input string) float32 {
	var errors int
	for i, char := range original {
		if i >= len(input) || byte(char) != input[i] {
			errors++
		}
	}
	return float32(errors) / float32(len(original))
}

