package main

import (

	"github.com/charmbracelet/bubbles/textinput"
)

type TypingTest struct {
	SampleText string
	UserInput  textinput.Model
}
type Result struct {
	WPM       int
	ErrorRate float32
}

type Model struct {
	styles       *Styles
	tests        []TypingTest
	width        int
	height       int
	currentState State
}
