package main

import (
	"fmt"
	"strconv"

	"github.com/charmbracelet/bubbles/textinput"
	"github.com/charmbracelet/lipgloss"
)
func startScreen(styles *Styles, width int, height int, state *StartState, tests []TypingTest) string {

	var items []string

	// Welcome Message
	welcome := "Welcome to the TeaTyping!\n\n"
	items = append(items, welcome)

	for i := range tests {
		optionStyle := lipgloss.NewStyle()

		// When the item is selected, we invert the colors
		if state.cursor == i {
			optionStyle = optionStyle.Copy().
				Background(lipgloss.Color("15")).
				Foreground(lipgloss.Color("00"))
		}

		// Add the test text, and apply the style
		item := fmt.Sprintf("Option %s", strconv.Itoa(i))
		styledItem := optionStyle.Render(item)
		items = append(items, styledItem)

		// Separation between items
		items = append(items, "")
	}

	// Joining the styled items
	content := lipgloss.JoinVertical(lipgloss.Left, items...)

	// Centering the content
	centered := lipgloss.Place(width, height, lipgloss.Center, lipgloss.Center, content)
	return centered
}

func typingView(sampleText string, userInput textinput.Model, styles *Styles, width int, height int) string {
	userInput.Prompt = ""
	userInput.CursorStart()
	userInputText := userInput.Value()
	displayedInput := ""

	notTypedStyle := lipgloss.NewStyle().Foreground(lipgloss.Color("8"))
	correctTypeStyle := lipgloss.NewStyle().Foreground(lipgloss.Color("15"))
	nextCharStyle := lipgloss.NewStyle().Background(lipgloss.Color("15")).Foreground(lipgloss.Color("0"))

	for i, char := range sampleText {
		if i < len(userInputText) {
			if userInputText[i] == byte(char) {
				displayedInput += correctTypeStyle.Render(string(char))
			} else {
				// ANSI color 1 as the background and ANSI color 0 as the foreground for incorrect characters
				errorStyle := lipgloss.NewStyle().
				//	Background(lipgloss.Color("15")).
					Foreground(lipgloss.Color("1")).
                    Underline(true)
				displayedInput += errorStyle.Render(string(char))
				//displayedInput += errorStyle.Render("*")
			}
		} else {
			if i == len(userInputText) {
				// Style the next character to be typed
				displayedInput += nextCharStyle.Render(string(char))
			} else {
				// If the user hasn't typed this character yet, render with notTypedStyle
				displayedInput += notTypedStyle.Render(string(char))
			}
		}
	}

	content := lipgloss.JoinVertical(
		lipgloss.Left,
		styles.InputField.Render(displayedInput),
	)
	centered := lipgloss.Place(width, height, lipgloss.Center, lipgloss.Center, content)
	return centered
}

func resultView(result Result, styles *Styles, width int, height int) string {
	// Result details
	text := fmt.Sprintf("WPM: %d\nError Rate: %.2f%%\n\n", result.WPM, result.ErrorRate*100)

	// Instructions
	instructions := lipgloss.NewStyle().Foreground(styles.BorderColor).Render("Press 'r' for restart or 'q' for quit")

	// Combine result details and instructions
	combined := text + instructions

	// Center combined content on the screen
	centered := lipgloss.Place(width, height, lipgloss.Center, lipgloss.Center, combined)

	return centered
}
