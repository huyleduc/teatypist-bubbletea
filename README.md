# TeaTypist

TeaTypist is a terminal-based typing speed and accuracy test application built using the Bubble Tea framework.

## Features

- Test your typing speed and accuracy with different samples.
- Terminal-based user interface for an immersive typing experience.
- Simple and intuitive interaction using keyboard shortcuts.

## Getting Started

1. Clone the repository:

    ```sh
    git clone https://gitlab.com/huyleduc/teatypist-bubbletea.git
    cd TeaTypist
    ```

2. Install dependencies:

    ```sh
    go get
    ```

3. Build and run the application:

    ```sh
    go run .
    ```

## Usage

- Use arrow keys or `j`/`k` to navigate through test options.
- Press `Enter` to start a typing test.
- After typing, press `Enter` to view your typing results.


