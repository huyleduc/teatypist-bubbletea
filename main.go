package main

import (
	"log"
"os"

	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"golang.org/x/term"
)
func NewModel()Model{

	inputModel := textinput.New()
	inputModel.Focus()

	tests := []TypingTest{
		{
			SampleText: "This is sample text A. Please type this accurately.",
			UserInput:  inputModel,
		},
		{
			SampleText: "Sample text B is a bit different. Give it a shot!",
			UserInput:  inputModel,
		},
	}

	styles := DefaultStyles()
				termWidth, termHeight, _ := term.GetSize(int(os.Stdin.Fd()))
	mainModel :=Model{
		tests:  tests,
		styles: styles,
        height: termHeight,
        width:termWidth,
    }


	mainModel.currentState = &StartState{Model: mainModel}
return mainModel

}

func main() {
    m := NewModel()
	p := tea.NewProgram(m, tea.WithAltScreen())

	if _, err := p.Run(); err != nil {
		log.Fatal(err)
	}
}

func (m Model) Init() tea.Cmd {
	return nil
}
func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
    switch msg := msg.(type) {
    case tea.WindowSizeMsg:
        m.width = msg.Width
        m.height = msg.Height
        return m, nil
    default:
        newState, cmd := m.currentState.Update(msg)
        if newState != nil {
            m.currentState = newState
        }
        return m, cmd
    }
}


func (m Model) View() string {
	return m.currentState.View()
}
